package com.trallerita.gtime.gtime.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.trallerita.gtime.gtime.R;
import com.trallerita.gtime.gtime.adaptadores.AdaptadorTarea;
import com.trallerita.gtime.gtime.modelos.Entorno;
import com.trallerita.gtime.gtime.modelos.Tarea;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

public class TareaActivity extends AppCompatActivity implements RealmChangeListener<Entorno> {

    private ListView listaTareas;
    private AdaptadorTarea adaptadorTarea;
    private RealmList<Tarea> ltareas;
    private RealmResults<Tarea> tareas;
    private Realm realm;
    private int idEntorno, horasPlanifInt, horasPlanifIntE;
    private Entorno entorno;
    Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarea);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        res = getResources();

        realm = Realm.getDefaultInstance();

        if (getIntent().getExtras() != null){
            idEntorno = getIntent().getExtras().getInt("idEntorno");
        }

        entorno = realm.where(Entorno.class).equalTo("id",idEntorno).findFirst();
        // Mira si hay cambios en la base de datos.
        entorno.addChangeListener(this);
        ltareas = entorno.getTareas();
        tareas = ltareas.sort("fechaCreacion", Sort.DESCENDING);

        // Cambiamos el título de la activity
        this.setTitle(entorno.getNombre());

        listaTareas = (ListView) findViewById(R.id.lvListaTareas);
        adaptadorTarea = new AdaptadorTarea(this,R.layout.list_tareas,tareas);
        listaTareas.setAdapter(adaptadorTarea);

        clickarElementoLista();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fbMasTarea);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertNuevaTarea(res.getString(R.string.tarea_nueva),res.getString(R.string.tarea_nueva_sub));

            }
        });

        // Para que funcione el editar / borrar ( manteniendo pulsado )
        registerForContextMenu(listaTareas);
    }

    // Método que crea un Dialog en la misma actividad para poder añadir un Entorno de Trabajo nuevo
    private void alertNuevaTarea(String titulo, String mensaje){

        final AlertDialog.Builder constructorAlert = new AlertDialog.Builder(this);

        if (titulo != null){
            constructorAlert.setTitle(titulo);
        }
        if (mensaje != null){
            constructorAlert.setMessage(mensaje);
        }

        View viewDialog = LayoutInflater.from(this).inflate(R.layout.dialog_nueva_tarea, null);
        constructorAlert.setView(viewDialog);

        final EditText nombreTareaNuevo = (EditText) viewDialog.findViewById(R.id.etNombreNuevaTarea);
        final CheckBox controlSubtarea = (CheckBox) viewDialog.findViewById(R.id.cBControlSubTarea);
        final CheckBox controlHoras = (CheckBox) viewDialog.findViewById(R.id.cbControlHorasPlanif);
        final EditText horasPlanif = (EditText) viewDialog.findViewById(R.id.etHorasFlanifInt);

        // Control de la selección del checkbox
        // Cuando detecta el cambio, muestra el editText para determinar las horas planificadas
        controlHoras.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    horasPlanif.setVisibility(View.VISIBLE);
                }else{
                    horasPlanif.setVisibility(View.INVISIBLE);
                }
            }
        });

        constructorAlert.setPositiveButton(res.getString(R.string.anyadir), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String codigoTarea = nombreTareaNuevo.getText().toString().trim();
                String horasPlanifIni = horasPlanif.getText().toString().trim();

                if (horasPlanifIni.equals("")){
                    horasPlanifInt = 0;
                }else{
                    horasPlanifInt = Integer.parseInt(horasPlanifIni);
                }

                if (codigoTarea.length() > 0){
                    crearNuevaTarea(codigoTarea, controlSubtarea.isChecked(), controlHoras.isChecked(), horasPlanifInt);
                }else{
                    Toast.makeText(getApplicationContext(), res.getString(R.string.id_tarea_vacio), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog dialog = constructorAlert.create();
        dialog.show();

    }

    private void alertEditarTarea(String titulo, String mensaje, final Tarea tarea){

        AlertDialog.Builder constructorAlert = new AlertDialog.Builder(this);

        if (titulo != null){
            constructorAlert.setTitle(titulo);
        }
        if (mensaje != null){
            constructorAlert.setMessage(mensaje);
        }

        View viewDialog = LayoutInflater.from(this).inflate(R.layout.dialog_nueva_tarea, null);
        constructorAlert.setView(viewDialog);

        final EditText nombreTareaNuevo = (EditText) viewDialog.findViewById(R.id.etNombreNuevaTarea);
        final CheckBox controlSubtarea = (CheckBox) viewDialog.findViewById(R.id.cBControlSubTarea);
        final CheckBox controlHoras = (CheckBox) viewDialog.findViewById(R.id.cbControlHorasPlanif);
        final EditText horasPlanif = (EditText) viewDialog.findViewById(R.id.etHorasFlanifInt);

        controlHoras.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    horasPlanif.setVisibility(View.VISIBLE);
                }else{
                    horasPlanif.setVisibility(View.INVISIBLE);
                }
            }
        });

        nombreTareaNuevo.setText(tarea.getCodigoTarea());
        controlSubtarea.setChecked(tarea.getSubtareascontrol());
        controlHoras.setChecked(tarea.getHorasPlanifControl());

        if (controlHoras.isChecked()){
            horasPlanif.setVisibility(View.VISIBLE);
            horasPlanif.setText(tarea.getHorasPlanif()+"");
        }

        constructorAlert.setPositiveButton(res.getString(R.string.editar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String codigoTarea = nombreTareaNuevo.getText().toString().trim();
                String horasPlanifEdi = horasPlanif.getText().toString().trim();

                if (horasPlanifEdi.equals("")){
                    horasPlanifIntE = 0;
                }else{
                    horasPlanifIntE = Integer.parseInt(horasPlanifEdi);
                }

                if (codigoTarea.length() == 0){
                    Toast.makeText(getApplicationContext(), res.getString(R.string.id_tarea_vacio), Toast.LENGTH_SHORT).show();
                }/*else if (codigoTarea.equals(tarea.getCodigoTarea())){
                    Toast.makeText(getApplicationContext(), res.getString(R.string.mismo_nombre_tarea), Toast.LENGTH_SHORT).show();
                }*/else{
                    editarTarea(tarea,codigoTarea, controlSubtarea.isChecked(), controlHoras.isChecked(), horasPlanifIntE);
                }

            /*   if (codigoTarea.length() > 0){
                    crearNuevaTarea(codigoTarea);
                }else{
                    Toast.makeText(getApplicationContext(), "Código de la tarea vacío", Toast.LENGTH_SHORT).show();
                } */
            }
        });

        AlertDialog dialog = constructorAlert.create();
        dialog.show();

    }

    /*
     Método que al clickar sobre un elemento de la lista,
        1. Si no tiene subtareas --> pantalla crono
        2. Si tiene subtareas --> pantalla lista subtareas
    */
    private void clickarElementoLista(){
        // Clickar sobre un elemento de la lista
        listaTareas.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Toast.makeText(TareaActivity.this,""+tareas.get(position),Toast.LENGTH_SHORT).show();

                if (tareas.get(position).getSubtareascontrol() == false){
                    Intent intent = new Intent(TareaActivity.this, CronoActivity.class);
                    intent.putExtra("idTarea",tareas.get(position).getId());
                    intent.putExtra("idEntorno",idEntorno);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(TareaActivity.this, SubTareaActivity.class);
                    intent.putExtra("idTarea",tareas.get(position).getId());
                    intent.putExtra("idEntorno",idEntorno);
                    startActivity(intent);
                }

            }

        });
    }

    // Método que insertará en la Base de Datos un nuevo Entorno de Trabajo
    private void crearNuevaTarea(String codigoTarea, boolean controlSubTarea, boolean controlHoras, int horasPlanifInt) {

        // Se hará mediante una transacción, para evitar problemas y errores.
        realm.beginTransaction();

        // Creamos el entorno y lo insertamos en la BD
        Tarea tarea = new Tarea(codigoTarea, controlSubTarea, controlHoras, horasPlanifInt);
        realm.copyToRealm(tarea);
        // De esta manera se guarda la relación para con el entorno.
        entorno.getTareas().add(tarea);

        // Fin transacción
        realm.commitTransaction();
    }

    private void borrarTodo(){
        realm.beginTransaction();
        entorno.getTareas().deleteAllFromRealm();
        realm.commitTransaction();
        // Reinicia la actividad, asi podemos ver los cambios al momento.
        //recreate();
    }

    private void borrarTarea(Tarea tarea){
        realm.beginTransaction();
        tarea.deleteFromRealm();
        realm.commitTransaction();
        //recreate();
    }

    private void editarTarea(Tarea tarea, String nombreNuevo, boolean controlSubTarea, boolean controlHoras, int horasPlanifEdit){
        realm.beginTransaction();
        tarea.setCodigoTarea(nombreNuevo);
        tarea.setSubtareascontrol(controlSubTarea);
        tarea.setHorasPlanifControl(controlHoras);
        tarea.setHorasPlanif(horasPlanifEdit);
        realm.copyToRealmOrUpdate(tarea);
        realm.commitTransaction();
        //recreate();
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        menu.setHeaderTitle(tareas.get(info.position).getCodigoTarea());
        getMenuInflater().inflate(R.menu.context_menu_tareas_activity, menu);

        //super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.borrar_entorno:
                borrarTarea(tareas.get(info.position));
                return true;
            case R.id.editar_entorno:
                alertEditarTarea(res.getString(R.string.editar_tarea),res.getString(R.string.editar_tarea_vamos),tareas.get(info.position));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tareas, menu);
        //return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                return true;
            case R.id.importar:
                importarCSV();
                return true;
            case R.id.borrar_todo:
                borrarTodo();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    // Método que redigirá a la pantalla para poder importar el CSV
    private void importarCSV() {

        Intent intent = new Intent(TareaActivity.this, ImportarActivity.class);
        intent.putExtra("idEntorno",idEntorno);
        startActivity(intent);

    }

    @Override
    public void onChange(Entorno element) {
        adaptadorTarea.notifyDataSetChanged();
    }
}
