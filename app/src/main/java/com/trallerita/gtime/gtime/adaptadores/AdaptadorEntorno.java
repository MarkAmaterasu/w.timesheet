package com.trallerita.gtime.gtime.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trallerita.gtime.gtime.R;
import com.trallerita.gtime.gtime.modelos.Entorno;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class AdaptadorEntorno extends BaseAdapter {

    private Context contexto;
    private int layout;
    private List<Entorno> entornos;

    public AdaptadorEntorno(Context contexto, int layout, List<Entorno> entornos){
        this.contexto = contexto;
        this.layout = layout;
        this.entornos = entornos;
    }

    @Override
    public int getCount() {
        return this.entornos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.entornos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        // Patrón View Holder
        ViewHolder holder;
        if (convertView == null){
            // Inflamos la vista que nos ha llegado con nuestro Layout personalizado.
            LayoutInflater vistaInflada = LayoutInflater.from(this.contexto);
            convertView = vistaInflada.inflate(R.layout.list_entornos,null);

            holder = new ViewHolder();
            // Referenciamos el elemento a modificar y lo rellenamos.
            holder.nombreEntorno = (TextView) convertView.findViewById(R.id.tvEntorno);
            holder.numTareas = (TextView) convertView.findViewById(R.id.tvNumTareas);
            holder.prioridad = (TextView) convertView.findViewById(R.id.ivFav);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        // Nos traemos el valor actual dependiente de la posición.
       /* String entornoActual = entornos.get(position);
        holder.nombreEntorno.setText(entornoActual);*/

        Entorno entorno = entornos.get(position);
        holder.nombreEntorno.setText(entorno.getNombre());
        holder.prioridad.setText(entorno.getPrioridad()+"");

        int numTareasInt = entorno.getTareas().size();
        String tvNumTareas = (numTareasInt == 1 ) ? numTareasInt + " tarea" : numTareasInt + " tareas";
        holder.numTareas.setText(tvNumTareas);

        // Formateo Fecha tipo DATE
        /* DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        String crearFecha = formatoFecha.format(entorno.getFechaCreacion());
        holder.fecha.setText(crearFecha); */

        // Devolvemos la vista
        return convertView;

    }

    static class ViewHolder {
        private TextView nombreEntorno;
        private TextView numTareas;
        private  TextView prioridad;
    }
}
