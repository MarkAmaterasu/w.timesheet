package com.trallerita.gtime.gtime.app;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.trallerita.gtime.gtime.modelos.Entorno;
import com.trallerita.gtime.gtime.modelos.SubTarea;
import com.trallerita.gtime.gtime.modelos.Tarea;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import java.util.concurrent.atomic.AtomicInteger;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by mark_ on 04/04/2017.
 */

public class Libreria extends Application {

    public static AtomicInteger entornoID = new AtomicInteger();
    public static AtomicInteger tareaID = new AtomicInteger();
    public static AtomicInteger subTareaID = new AtomicInteger();

    // Siempre se ejecutará antes del Main Activity
    @Override
    public void onCreate() {
        super.onCreate();

        realmConfig();

        Realm realm = Realm.getDefaultInstance();
        entornoID = getIdByTable(realm, Entorno.class);
        tareaID = getIdByTable(realm, Tarea.class);
        subTareaID = getIdByTable(realm, SubTarea.class);


        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());


        realm.close();

    }

    private void realmConfig(){

        Realm.init(getApplicationContext());

        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);


    }

    // Método para autoincrementar el campo ID de las tareas, Realm no lo tiene implementado aun.
    private <T extends RealmObject> AtomicInteger getIdByTable(Realm realm, Class<T> anyClass){

        // BD donde la table ( clase ) y que lo encuentretodo ( Select * )
        RealmResults<T> resultados = realm.where(anyClass).findAll();

        return (resultados.size() > 0) ? new AtomicInteger(resultados.max("id").intValue()) : new AtomicInteger();

    }

}
