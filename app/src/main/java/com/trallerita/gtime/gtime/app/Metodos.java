package com.trallerita.gtime.gtime.app;

import android.os.Bundle;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by mark_ on 26/04/2017.
 */

public class Metodos {

    private static int listaTiempo[] = new int[3];

    // Método que recoge el valor del crono en milisegundos y los pasará a horas / minutos
    public static int[] milisegundosAHora(long tiempoDesarrollo){

        int horas = (int) (tiempoDesarrollo / 3600000);
        int minutos = (int) (tiempoDesarrollo - horas * 3600000) / 60000;
        int segundos = (int) (tiempoDesarrollo - horas * 3600000 - minutos * 60000) / 1000;

        listaTiempo[0] = horas;
        listaTiempo[1] = minutos;
        listaTiempo[2] = segundos;

        return listaTiempo;
    }

    public static void imprimirConsola(Bundle savedInstanceStateVar, String descripcion, String aImprimir){
        /*
        * Para depurar. Y ver por consola.
        * Ejecutar en modo debugger.
        * */
        if(savedInstanceStateVar != null){
            Log.d("Estado", savedInstanceStateVar.toString());
        }

        Log.d("********************","**********************");
        Log.d(descripcion, aImprimir);
        Log.d("********************","**********************");

     }

    /**
     *
     * @return yyyy-MM-dd HH:mm:ss formate date as string
     */
    public static String getCurrentTimeStamp(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTimeStamp = dateFormat.format(new Date()); // Find todays date

            currentTimeStamp = currentTimeStamp.replace(" ","");
            currentTimeStamp = currentTimeStamp.replace("-","_");
            currentTimeStamp = currentTimeStamp.replace(":","");

            return currentTimeStamp;

        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

}
