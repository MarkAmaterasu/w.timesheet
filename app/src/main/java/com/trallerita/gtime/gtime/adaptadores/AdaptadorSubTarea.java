package com.trallerita.gtime.gtime.adaptadores;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.trallerita.gtime.gtime.R;
import com.trallerita.gtime.gtime.app.Metodos;
import com.trallerita.gtime.gtime.modelos.SubTarea;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import static com.trallerita.gtime.gtime.activities.SubTareaActivity.numSubTareasOK;
import static com.trallerita.gtime.gtime.activities.SubTareaActivity.numSubTareasTot;

/**
 * Created by mark_ on 23/04/2017.
 */

public class AdaptadorSubTarea extends BaseAdapter {

    private Context contexto;
    private int layout;
    private List<SubTarea> subTareas;
    private int[] listaTiempo = new int[3];
    private String horas, minutos, segundos, horasLargo, minutosLargo;

    public AdaptadorSubTarea(Context contexto, int layout, List<SubTarea> subTareas){
        this.contexto = contexto;
        this.layout = layout;
        this.subTareas = subTareas;
    }


    @Override
    public int getCount() {
        return subTareas.size();
    }

    @Override
    public Object getItem(int position) {
        return subTareas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        AdaptadorSubTarea.ViewHolder holder;

        if (convertView == null){

            convertView = LayoutInflater.from(contexto).inflate(layout, null);
            holder = new AdaptadorSubTarea.ViewHolder();
            holder.codigoSubTarea = (TextView) convertView.findViewById(R.id.tvCodigoSubTarea);
            holder.fechaTarea = (TextView) convertView.findViewById(R.id.tvFechaSubTarea);
            holder.horasDesMashorasPlan = (TextView) convertView.findViewById(R.id.tvHorasTarea);
            holder.imagenFinish = (ImageView) convertView.findViewById(R.id.ivfinish);
            convertView.setTag(holder);

        }else{

            holder = (AdaptadorSubTarea.ViewHolder) convertView.getTag();

        }

        SubTarea subTarea = subTareas.get(position);

        holder.codigoSubTarea.setText(subTarea.getCodigoSubTarea());
        numSubTareasTot += 1;
        if (subTarea.getEstadoSubTarea().equals("OK")){
            numSubTareasOK += 1;
            // Cambia el color del icono.
            holder.imagenFinish.setImageTintList(ColorStateList.valueOf(Color.BLUE));
        }else if(subTarea.getEstadoSubTarea().equals("En curso")){
            holder.imagenFinish.setImageTintList(ColorStateList.valueOf(Color.RED));
        }

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = df.format(subTarea.getFechaCreacion());
        holder.fechaTarea.setText(fecha);

        // En caso de que no se haya empezado el desarrollo que no imprima nada por pantalla.
        if (subTarea.getTiempoDesarrollo() == 0){
            holder.horasDesMashorasPlan.setText("");
        }else if (subTarea.getTiempoDesarrollo() != 0){ // Si ha empezado, que imprima
            // Transforma el tiempo en milisegundos de la BD en horas/minutos/segundos
            listaTiempo = Metodos.milisegundosAHora(subTarea.getTiempoDesarrollo());
            // Convertimos los números en string para poder mirar su longitud
            horas = String.valueOf(listaTiempo[0]);
            minutos = String.valueOf(listaTiempo[1]);
            segundos = String.valueOf(listaTiempo[2]);
            horasLargo = String.valueOf(horas.length());
            minutosLargo = String.valueOf(minutos.length());

            // En el caso de que su longitud sea de 1, añadimos un 0 delante.
            if (horasLargo.equals("1")){
                horas = String.format("%02d",listaTiempo[0]);
            }
            if (minutosLargo.equals("1")){
                minutos = String.format("%02d",listaTiempo[1]);
            }

            // Comprobamos si es una tarea con horas planificadas
            // Si es así, imprimimos el tiempo desarrollado junto con el tiempo planificado
            if (subTarea.getHorasPlanifControl()){
                holder.horasDesMashorasPlan.setText(horas+":"+minutos+" / "+subTarea.getHorasPlanif()+"H");
            }else{
                // En caso contrario, solo el tiempo desarrollado
                holder.horasDesMashorasPlan.setText(horas+":"+minutos);
            }
        }

        return convertView;
    }

    public class ViewHolder{

        TextView codigoSubTarea;
        TextView fechaTarea;
        TextView horasDesMashorasPlan;
        ImageView imagenFinish;

    }


}