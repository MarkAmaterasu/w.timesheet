package com.trallerita.gtime.gtime.adaptadores;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.trallerita.gtime.gtime.R;
import com.trallerita.gtime.gtime.app.Metodos;
import com.trallerita.gtime.gtime.modelos.Tarea;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by mark_ on 08/04/2017.
 */

public class AdaptadorTarea extends BaseAdapter {

    private Context contexto;
    private int layout;
    private List<Tarea> tareas;
    private int[] listaTiempo = new int[3];
    private String horas, minutos, segundos, horasLargo, minutosLargo;
    private Tarea tarea;
    private ViewHolder holder;
    private long sumaTiempoSubtareas;


    public AdaptadorTarea(Context contexto, int layout, List<Tarea> tareas){
        this.contexto = contexto;
        this.layout = layout;
        this.tareas = tareas;
    }


    @Override
    public int getCount() {
        return tareas.size();
    }

    @Override
    public Object getItem(int position) {
        return tareas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //ViewHolder holder;

        if (convertView == null){

            convertView = LayoutInflater.from(contexto).inflate(layout, null);
            holder = new ViewHolder();
            holder.codigoTarea = (TextView) convertView.findViewById(R.id.tvCodigoTarea);
            holder.fechaTarea = (TextView) convertView.findViewById(R.id.tvFechaTarea);
            holder.numSubTareas = (TextView) convertView.findViewById(R.id.tvNumSubTareas);
            holder.horasDesMashorasPlan = (TextView) convertView.findViewById(R.id.tvHorasTarea);
            holder.imagenFinish = (ImageView) convertView.findViewById(R.id.ivfinish);
            convertView.setTag(holder);

        }else{

            holder = (ViewHolder) convertView.getTag();

        }

        tarea = tareas.get(position);

        holder.codigoTarea.setText(tarea.getCodigoTarea());

        if (tarea.getEstadoTarea().equals("OK")){
            // Cambia el color del icono.
            holder.imagenFinish.setImageTintList(ColorStateList.valueOf(Color.BLUE));
        }else if(tarea.getEstadoTarea().equals("En curso")){
            holder.imagenFinish.setImageTintList(ColorStateList.valueOf(Color.RED));
        }

        // Transformamos el formato de la fecha
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = df.format(tarea.getFechaCreacion());
        holder.fechaTarea.setText(fecha);

        // Cuenta el número de subTareas de una tarea en particular
        int numSubTareasInt = tarea.getSubtareas().size();
        String tvNumSubTareas = (numSubTareasInt == 1 ) ? numSubTareasInt + " Subtarea" : numSubTareasInt + " Subtareas";
        // En el caso de que la tarea no pueda tener subtareas, no se muestra nada.
        if (tarea.getSubtareascontrol() == false){
            holder.numSubTareas.setText("");
        }else{
            holder.numSubTareas.setText(tvNumSubTareas);
        }

        // En caso de que no se haya empezado el desarrollo que no imprima nada por pantalla.
        if (tarea.getTiempoDesarrollo() == 0 && numSubTareasInt != 0){
            //holder.horasDesMashorasPlan.setText("");
            for (int i = 0; i < numSubTareasInt; i++){
                long tiempoindiSubTarea = tarea.getSubtareas().get(i).getTiempoDesarrollo();
                sumaTiempoSubtareas = sumaTiempoSubtareas + tiempoindiSubTarea;
            }

            if (sumaTiempoSubtareas != 0 ){
                imprimirTiempoDesarrollo(sumaTiempoSubtareas);
                sumaTiempoSubtareas = 0;
            }

        }else if (tarea.getTiempoDesarrollo() != 0 ){ // Si ha empezado, que imprima

            imprimirTiempoDesarrollo(tarea.getTiempoDesarrollo());

        }else{

            holder.horasDesMashorasPlan.setText("");

        }

        return convertView;
    }

    private void imprimirTiempoDesarrollo(long tiempo){

        // Transforma el tiempo en milisegundos de la BD en horas/minutos/segundos
        listaTiempo = Metodos.milisegundosAHora(tiempo);
        // Convertimos los números en string para poder mirar su longitud
        horas = String.valueOf(listaTiempo[0]);
        minutos = String.valueOf(listaTiempo[1]);
        segundos = String.valueOf(listaTiempo[2]);
        horasLargo = String.valueOf(horas.length());
        minutosLargo = String.valueOf(minutos.length());

        // En el caso de que su longitud sea de 1, añadimos un 0 delante.
        if (horasLargo.equals("1")){
            horas = String.format("%02d",listaTiempo[0]);
        }
        if (minutosLargo.equals("1")){
            minutos = String.format("%02d",listaTiempo[1]);
        }

        // Comprobamos si es una tarea con horas planificadas
        // Si es así, imprimimos el tiempo desarrollado junto con el tiempo planificado
        if (tarea.getHorasPlanifControl()){
            holder.horasDesMashorasPlan.setText(horas+":"+minutos+" / "+tarea.getHorasPlanif()+"H");
        }else{
            // En caso contrario, solo el tiempo desarrollado
            holder.horasDesMashorasPlan.setText(horas+":"+minutos);
        }

    }

    private class ViewHolder{

        TextView codigoTarea;
        TextView fechaTarea;
        TextView numSubTareas;
        TextView horasDesMashorasPlan;
        ImageView imagenFinish;

    }


}
