package com.trallerita.gtime.gtime.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.trallerita.gtime.gtime.R;
import com.trallerita.gtime.gtime.app.Metodos;
import com.trallerita.gtime.gtime.modelos.SubTarea;
import com.trallerita.gtime.gtime.modelos.Tarea;

import io.realm.Realm;

import static android.R.drawable.presence_online;


public class CronoActivity extends AppCompatActivity  {

    private Realm realm;
    private int idTarea, numParam, idTareaSubtarea, idSubtarea, idEntorno, numSubTareasOK, numSubTareasTot;
    private int numParametrosParaSoloTarea = 2;
    private int contadorCrono = 0, contadorTiempoCrono = 0;
    private Tarea tarea;
    private SubTarea subTarea;
    private Chronometer crono;
    private Chronometer crono2;
    // Tiempo en milisegundos. A TENER EN CUENTA
    private long tiempo;
    private boolean controlViewCrono;
    FloatingActionButton fabPlay, fabPause, fabCancel, fabEnd;
    private String estadoTarea;
    private Bundle savedInstance;
    private Drawable imagenCronoTBLR;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crono);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Banner Top
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        //Cargamos el interstitial
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3378595723588109/1261897165");

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        requestNewInterstitial();

       /* // Interstitial Ad
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3378595723588109/1261897165");
        //final AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        //adRequestBuilder.addTestDevice("your test device id goes here");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                //mInterstitialAd.loadAd(adRequestBuilder.build());
            }
        });
        mInterstitialAd.loadAd(new AdRequest.Builder().build());*/

        savedInstance = savedInstanceState;
        imagenCronoTBLR = ResourcesCompat.getDrawable(getResources(),presence_online,null);

        realm = Realm.getDefaultInstance();

        if (getIntent().getExtras() != null){
            // Cuenta el número de parámetros que se le pasa al cargar esta pantalla.
            numParam = getIntent().getExtras().size();

            if (esTarea()){
                // Recogemos los datos de la pantalla de la lista de tareas
                idTarea = getIntent().getExtras().getInt("idTarea");
                idEntorno = getIntent().getExtras().getInt("idEntorno");

                // Consulta a la tabla de tareas,
                tarea = realm.where(Tarea.class).equalTo("id", idTarea).findFirst();
                // Cambiamos el título de la activity
                this.setTitle(tarea.getCodigoTarea());
                // Asignamos el tiempo de la base de datos.
                tiempo = tarea.getTiempoDesarrollo();
                estadoTarea = tarea.getEstadoTarea();

            }else{
                // Recogemos los datos de la pantalla del listado de subtareas
                idTareaSubtarea = getIntent().getExtras().getInt("idTarea");
                idSubtarea = getIntent().getExtras().getInt("idSubTarea");
                idEntorno = getIntent().getExtras().getInt("idEntorno");
                numSubTareasOK = 0;
                numSubTareasOK = getIntent().getExtras().getInt("numSubTareasOK");
                numSubTareasTot = getIntent().getExtras().getInt("numSubTareasTot");

                // Consulta a la tabla de tareas.
                tarea = realm.where(Tarea.class).equalTo("id", idTareaSubtarea).findFirst();

                // Consulta a la tabla de subtareas.
                subTarea = realm.where(SubTarea.class).equalTo("id",idSubtarea).findFirst();
                this.setTitle(subTarea.getCodigoSubTarea());
                tiempo = subTarea.getTiempoDesarrollo();
                estadoTarea = subTarea.getEstadoSubTarea();

            }

        }

        crono = (Chronometer) findViewById(R.id.crono);
        crono2 = (Chronometer) findViewById(R.id.crono2);
        fabPlay = (FloatingActionButton) findViewById(R.id.fbPlay);
        fabPause = (FloatingActionButton) findViewById(R.id.fbPause);
        fabCancel = (FloatingActionButton) findViewById(R.id.fbCancel);
        fabEnd = (FloatingActionButton) findViewById(R.id.fbEnd);

        // Método para cambiar las imagenes del cronometro.
        imagesCrono();

        // Segun el estado de la tarea mostraremos el boton de finalizar tarea o la de renaudar.
        if (estadoTarea.equals("OK")){
            fabPlay.setVisibility(View.INVISIBLE);
            fabPause.setVisibility(View.INVISIBLE);
            fabEnd.setVisibility(View.INVISIBLE);
        }else{
            fabCancel.setVisibility(View.INVISIBLE);
        }

        crono2.setBase(SystemClock.elapsedRealtime() - tiempo);

        crono.setVisibility(View.INVISIBLE);
        controlViewCrono = false;

        fabPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                contadorTiempoCrono = 0;
                fabPlay.setVisibility(View.INVISIBLE);
                fabPause.setVisibility(View.VISIBLE);
                crono2.setVisibility(View.INVISIBLE);
                cronoStart();
            }
        });

        fabPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                contadorTiempoCrono = 1;
                fabPause.setVisibility(View.INVISIBLE);
                fabPlay.setVisibility(View.VISIBLE);
                cronoStop();

            }
        });

        fabCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fabPlay.setVisibility(View.VISIBLE);
                fabEnd.setVisibility(View.VISIBLE);
                fabCancel.setVisibility(View.INVISIBLE);
                reanudaTarea(estadoTarea);

            }
        });

        fabEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            fabPause.setVisibility(View.INVISIBLE);
            fabPlay.setVisibility(View.INVISIBLE);
            fabCancel.setVisibility(View.VISIBLE);
            fabEnd.setVisibility(View.INVISIBLE);
            completaTarea();
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }

            }
        });

    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    // Método para cambiar las imagenes del cronometro.
    private void imagesCrono(){

        crono.setCompoundDrawablesWithIntrinsicBounds(null,null,null, null);
        crono.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                switch (contadorCrono){
                    case 0: contadorCrono += 1;
                        crono.setCompoundDrawablesWithIntrinsicBounds(null,null,null, null);
                        break;
                    case 1: contadorCrono += 1;
                        crono.setCompoundDrawablesWithIntrinsicBounds(imagenCronoTBLR,null,null, null);
                        break;
                    case 2: contadorCrono += 1;
                        crono.setCompoundDrawablesWithIntrinsicBounds(imagenCronoTBLR,imagenCronoTBLR,null, null);
                        break;
                    case 3: contadorCrono += 1;
                        crono.setCompoundDrawablesWithIntrinsicBounds(imagenCronoTBLR,imagenCronoTBLR,imagenCronoTBLR, null);
                        break;
                    case 4: contadorCrono += 1;
                        crono.setCompoundDrawablesWithIntrinsicBounds(imagenCronoTBLR,imagenCronoTBLR,imagenCronoTBLR, imagenCronoTBLR);
                        break;
                    case 5: contadorCrono += 1;
                        crono.setCompoundDrawablesWithIntrinsicBounds(null,imagenCronoTBLR,imagenCronoTBLR, imagenCronoTBLR);
                        break;
                    case 6: contadorCrono += 1;
                        crono.setCompoundDrawablesWithIntrinsicBounds(null,null,imagenCronoTBLR, imagenCronoTBLR);
                        break;
                    case 7: contadorCrono = 0;
                        crono.setCompoundDrawablesWithIntrinsicBounds(null,null,null, imagenCronoTBLR);
                        break;
                    default:break;
                }
            }
        });

    }

    private void cronoStop(){

        tiempo = SystemClock.elapsedRealtime() - crono.getBase();
        actualizaTiempoDesarrollo(tiempo);
        crono.stop();

    }

    private void reanudaTarea(String estadoTarea){

        if (esTarea()){
            if (estadoTarea.equals("OK")){
                realm.beginTransaction();

                tarea.setEstadoTarea("En curso");
                realm.copyToRealmOrUpdate(tarea);

                realm.commitTransaction();
            }

        }else{
            if (estadoTarea.equals("OK")){
                realm.beginTransaction();

                subTarea.setEstadoSubTarea("En curso");
                realm.copyToRealmOrUpdate(subTarea);

                realm.commitTransaction();
                realm.beginTransaction();

                tarea.setEstadoTarea("En curso");
                realm.copyToRealmOrUpdate(tarea);

                realm.commitTransaction();
            }

        }

    }

    private void completaTarea(){

            if(esTarea()){

                realm.beginTransaction();

                tarea.setEstadoTarea("OK");
                realm.copyToRealmOrUpdate(tarea);

                realm.commitTransaction();

                Intent intent = new Intent(CronoActivity.this, TareaActivity.class);
                intent.putExtra("idEntorno",idEntorno);
                startActivity(intent);
                finish();

            }else{

                numSubTareasOK += 1;
                if (tareaCompletada()){

                    realm.beginTransaction();

                    tarea.setEstadoTarea("OK");
                    realm.copyToRealmOrUpdate(tarea);

                    realm.commitTransaction();

                }

                realm.beginTransaction();

                subTarea.setEstadoSubTarea("OK");
                realm.copyToRealmOrUpdate(subTarea);

                realm.commitTransaction();

                Intent intent = new Intent(CronoActivity.this, SubTareaActivity.class);
                intent.putExtra("idTarea",tarea.getId());
                intent.putExtra("idEntorno",idEntorno);
                startActivity(intent);
                finish();

            }

    }

    private void cronoStart(){

        if(esTarea()){

            if (tarea.getEstadoTarea().equals("Inactiva")){
                realm.beginTransaction();

                tarea.setEstadoTarea("En curso");
                realm.copyToRealmOrUpdate(tarea);

                realm.commitTransaction();
            }

        }else{

            if (subTarea.getEstadoSubTarea().equals("Inactiva")){
                realm.beginTransaction();

                subTarea.setEstadoSubTarea("En curso");
                realm.copyToRealmOrUpdate(subTarea);

                tarea.setEstadoTarea("En curso");
                realm.copyToRealmOrUpdate(tarea);

                realm.commitTransaction();
            }
        }

        if (controlViewCrono == false){
            controlViewCrono = true;
            crono.setVisibility(View.VISIBLE);
            if (tiempo != 0){
                crono.setBase(SystemClock.elapsedRealtime() - tiempo);
                crono.start();
            }else if(tiempo == 0){
                cronoRestart();
                crono.start();
            }
        }else{
            if (tiempo != 0){
                crono.setBase(SystemClock.elapsedRealtime() - tiempo);
                crono.start();
            }else if(tiempo == 0){
                cronoRestart();
                crono.start();
            }
        }

    }

    //  Método que inserta en la base de datos el tiempo desarrollado en una tarea.
    private void actualizaTiempoDesarrollo(long tiempoDesarrollo){

        // Comienza transacción
        realm.beginTransaction();

        if (esTarea()){
            tarea.setTiempoDesarrollo(tiempoDesarrollo);
            realm.copyToRealmOrUpdate(tarea);
        }else{
            subTarea.setTiempoDesarrollo(tiempoDesarrollo);
            realm.copyToRealmOrUpdate(subTarea);
        }

        realm.commitTransaction();
    }

    private boolean esTarea(){

        // Comprobamos si estamos sobre una tarea, o una subtarea.
        // @numParam = 2   --> Tarea
        // @numParam != 2  --> Subtarea
        if (numParam == numParametrosParaSoloTarea){
            return true;
        }else{
            return false;
        }

    }

    private boolean tareaCompletada(){
        if (numSubTareasOK == numSubTareasTot){
            return true;
        }else{
            return false;
        }
    }

    private void cronoRestart(){

        // setBase determina el tiempo de base del cronómetro.
        // En este caso lo pone a cero.
        crono.setBase(SystemClock.elapsedRealtime());

    }

    @Override
    public void onBackPressed() {

        // Para evitar que sobrescriba el tiempo en caso de dar pausa antes de dar atrás.
        if (contadorTiempoCrono == 0) {
            // Nos aseguramos que cuando le demos atrás guarde el tiempo que ha avanzado.
            if (controlViewCrono == false) {
            } else {

                tiempo = SystemClock.elapsedRealtime() - crono.getBase();
                actualizaTiempoDesarrollo(tiempo);
                cronoStop();

            }
        }

        super.onBackPressed();

    }


}
