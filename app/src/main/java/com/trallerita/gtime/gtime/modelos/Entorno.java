package com.trallerita.gtime.gtime.modelos;

import com.trallerita.gtime.gtime.app.Libreria;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by mark_ on 04/04/2017.
 */

public class Entorno extends RealmObject {

    // Valor para la BD con anotación Java
    @PrimaryKey
    private int id;
    @Required
    private String nombre;
    @Required
    private Date fechaCreacion;
    // Necesario para la relación entre entorno / tarea
    private RealmList<Tarea> tareas;
    private int prioridad;

    // Realm exige siempre tener un contructor vacío
    public Entorno(){

    }

    public Entorno (String nombre){
        this.id = Libreria.entornoID.incrementAndGet();
        this.nombre = nombre;
        this.fechaCreacion = new Date();
        this.tareas = new RealmList<Tarea>();
        this.prioridad = 0;
    }

    public Entorno (String nombre, int prioridad){
        this.id = Libreria.entornoID.incrementAndGet();
        this.nombre = nombre;
        this.fechaCreacion = new Date();
        this.tareas = new RealmList<Tarea>();
        this.prioridad = prioridad;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public RealmList<Tarea> getTareas() {
        return tareas;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }
}
