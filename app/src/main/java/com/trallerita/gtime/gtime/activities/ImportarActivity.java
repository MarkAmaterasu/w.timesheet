package com.trallerita.gtime.gtime.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.opencsv.CSVWriter;
import com.trallerita.gtime.gtime.DatePickerFragment;
import com.trallerita.gtime.gtime.DatePickerFragment2;
import com.trallerita.gtime.gtime.R;
import com.trallerita.gtime.gtime.app.Metodos;
import com.trallerita.gtime.gtime.modelos.Entorno;
import com.trallerita.gtime.gtime.modelos.Tarea;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class ImportarActivity extends AppCompatActivity {

    private RealmList<Tarea> tareas;
    private Realm realm;
    private Entorno entorno;
    private int idEntorno, longSize;
    private List<String[]> listadoTarea = new ArrayList<>();
    String nombreCarpeta = "GTime", nombreCSV, nombreEntorno;
    int listaTiempo[] = new int[3], mesActual;
    RadioGroup checkPeriodoGrupo;
    Bundle savedInstanceStateVar;
    RadioButton radioActual, radioPeriodo;
    Button btFechaInicial, btFechaFinal;
    Boolean conPeriodo;
    Date dateFechaInicial, dateFechaFinal;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1 ;
    private InterstitialAd mInterstitialAd;
    Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_importar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        res = getResources();

       /* mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3378595723588109/8992568591");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());*/

        // Permisos
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

            }
        }

        realm = Realm.getDefaultInstance();

        // Cambiamos el título de la activity
        this.setTitle(res.getString(R.string.exportar_csv));

        savedInstanceStateVar = savedInstanceState;

        if (getIntent().getExtras() != null){
            idEntorno = getIntent().getExtras().getInt("idEntorno");
        }

        final LinearLayout layoutBotones = (LinearLayout) findViewById(R.id.linearBotonesPeriodo);
        checkPeriodoGrupo = (RadioGroup) findViewById(R.id.checkGroupPeriodo);
        checkPeriodoGrupo.check(R.id.rBTMesActual);
        checkPeriodoGrupo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch(checkedId) {
                    case R.id.rBTMesActual:
                        layoutBotones.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.rBTPeriodo:
                        layoutBotones.setVisibility(View.VISIBLE);
                        break;
                }
            }

        });

    }

    // Método para los permisos de escritura android > 6
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {



                } else {


                }
                return;
            }
        }
    }

    // Método que crea el directorio siempre y cuando no exista.
    public void creaDirectorio(){

        File f = new File(Environment.getExternalStorageDirectory() + File.separator + nombreCarpeta);

        // Comprobamos si la carpeta está ya creada
        // Si la carpeta no está creada, la creamos.
        if(!f.isDirectory()) {
            String newFolder = File.separator + nombreCarpeta;
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File myNewFolder = new File(extStorageDirectory + newFolder);
            myNewFolder.mkdir(); //creamos la carpeta
        }else{
            //Log.d(TAG,"La carpeta ya estaba creada");
        }

    }

    // Método que crea el fichero CSV.
    public void creaFicheroCSV(Boolean conPeriodo, String nombreCSV){

        // Consulta a la base de datos.
        entorno = realm.where(Entorno.class).equalTo("id",idEntorno).findFirst();
        tareas = entorno.getTareas();
        longSize = tareas.size();
        nombreEntorno = entorno.getNombre();

        // Configuración del archivo CSV.
        String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        String fileName = nombreFicheroCSV(nombreCSV);
        String filePath = baseDir + File.separator + nombreCarpeta +  File.separator + fileName;
        File f = new File(filePath );
        CSVWriter writer = null;
        FileWriter mFileWriter = null;

        //Metodos.imprimirConsola(savedInstanceState,"ERROR -->", File.separator);

        // File exist
        if(f.exists() && !f.isDirectory()){
            try {
                mFileWriter = new FileWriter(filePath , true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            writer = new CSVWriter(mFileWriter);
        }
        else {
            try {
                writer = new CSVWriter(new FileWriter(filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Añadimos la cabecera a la lista.
        String[] cabecera = new String[]{res.getString(R.string.tarea),res.getString(R.string.tiempo_dedicado),res.getString(R.string.fecha_creacion)};
        listadoTarea.add(cabecera);

        // Recorremos los datos del entorno seleccionado.
        for (int i= 0; i < longSize ; i++){

            // Tareas con ControlSubTareas ACTIVO y que tienen SubTareas creadas
            //if (tareas.get(i).getSubtareascontrol() == true && tareas.get(i).getSubtareas().size() != 0){
                //Metodos.imprimirConsola(savedInstanceState,"Tareas -->", String.valueOf(tareas.get(i).getSubtareas()));
            //}else{
                String nombreTarea = tareas.get(i).getCodigoTarea();
                listaTiempo = Metodos.milisegundosAHora(tareas.get(i).getTiempoDesarrollo());
                String tiempoDesarrollo = String.valueOf(listaTiempo[0] + ":" + String.valueOf(listaTiempo[1]));
                // Transformamos el formato de la fecha
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String fechaString = df.format(tareas.get(i).getFechaCreacion());
                // Comprueba si se ha elegido con periodo o no. Y se formatean las fechas según el periodo.
                if (conPeriodo){
                    Date fechaDate = tareas.get(i).getFechaCreacion();
                    String dateFechaInicialString = df.format(dateFechaInicial);
                    String dateFechaFinalString = df.format(dateFechaFinal);

                    Metodos.imprimirConsola(savedInstanceStateVar,"Fecha BD String ---> ",fechaString);
                    Metodos.imprimirConsola(savedInstanceStateVar,"Fecha Periodo inicial ---> ",dateFechaInicialString);
                    Metodos.imprimirConsola(savedInstanceStateVar,"Fecha periodo final ---> ",dateFechaFinalString);

                    if (fechaDate.before(dateFechaFinal) && fechaDate.after(dateFechaInicial)){
                        String[] data = new String[]{nombreTarea,tiempoDesarrollo,fechaString};
                        listadoTarea.add(data);
                    }
                    if(fechaString.equals(dateFechaFinalString) || fechaString.equals(dateFechaInicialString)){
                        String[] data = new String[]{nombreTarea,tiempoDesarrollo,fechaString};
                        listadoTarea.add(data);
                    }

                }else {
                    // Coge número del mes de la tarea de la base de datos.
                    String mesFechaNum = fechaString.substring(3,5);
                    int numMesRealm = Integer.parseInt(mesFechaNum);
                    // Comprueba que sea el mismo mes, y lo inserta en la lista.
                    if (numMesRealm == mesActual){
                        String[] data = new String[]{nombreTarea,tiempoDesarrollo,fechaString};
                        listadoTarea.add(data);
                    }
                }
           // }
        }

        // Comprueba que no es null y escribe los datos en el fichero.
        assert writer != null;
        writer.writeAll(listadoTarea);
        listadoTarea.clear();
        Toast.makeText(getApplicationContext(), res.getString(R.string.csv_creado), Toast.LENGTH_SHORT).show();
        // Retornamos a la pantalla con la lista de tareas.
        Intent intent = new Intent(ImportarActivity.this, TareaActivity.class);
        intent.putExtra("idEntorno",idEntorno);
        startActivity(intent);

        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // Método que asigna el nombre al archi CSV
    public String nombreFicheroCSV(String nombreCSV){

        return nombreEntorno + "_" + nombreCSV + ".csv";

    }

    // Métodos que muestran un DatePicker en un dialog.
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void showDatePickerDialog2(View v) {
        DialogFragment newFragment = new DatePickerFragment2();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    // Acción de clickar el botón de Exportar a CSV.
    public void exportarOnClick(View v) throws ParseException {

        /*if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }*/

        radioActual = (RadioButton) findViewById(R.id.rBTMesActual);
        radioPeriodo = (RadioButton) findViewById(R.id.rBTPeriodo);
        btFechaInicial = (Button) findViewById(R.id.btFechaInicial);
        btFechaFinal = (Button) findViewById(R.id.btFechaFinal);

        creaDirectorio();

        // Controla el RadioButton seleccionado.
        if (radioActual.isChecked()){

            conPeriodo = false;
            // Recogemos el mes actual con valor númerico.
            Calendar calendario = Calendar.getInstance();
            mesActual = calendario.get(Calendar.MONTH);
            mesActual = mesActual + 1;
            // Asigna el nombre al archivo CSV que queremos crear con la fecha actual.
            nombreCSV = Metodos.getCurrentTimeStamp();

            creaFicheroCSV(conPeriodo, nombreCSV);

        }else if (radioPeriodo.isChecked()){

            String textoBotonInicial = (String) btFechaInicial.getText();
            String textoBotonFinal = (String) btFechaFinal.getText();
            conPeriodo = true;

            // Comprueba que se les ha asignado un valor a las fechas.
            if (textoBotonInicial.equals("Fecha inicial") | textoBotonFinal.equals("Fecha final")){

                Toast.makeText(getApplicationContext(), res.getString(R.string.asignar_periodo), Toast.LENGTH_SHORT).show();

            }else {

                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

                // Damos formato fecha al texto de los botones
                dateFechaInicial = df.parse(textoBotonInicial);
                dateFechaFinal = df.parse(textoBotonFinal);
                // Comprueba que la fecha final no es menor que la fecha inicial.
                if (dateFechaInicial.before(dateFechaFinal) | dateFechaInicial.equals(dateFechaFinal) ){

                    nombreCSV = Metodos.getCurrentTimeStamp();

                    creaFicheroCSV(conPeriodo, nombreCSV);

                }else{
                    Toast.makeText(getApplicationContext(), res.getString(R.string.error_periodo), Toast.LENGTH_SHORT).show();
                }

            }

        }else{
            Toast.makeText(getApplicationContext(),res.getString(R.string.asignar_periodo), Toast.LENGTH_SHORT).show();
        }

    }


}
