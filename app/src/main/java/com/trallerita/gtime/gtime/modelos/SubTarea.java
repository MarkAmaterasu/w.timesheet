package com.trallerita.gtime.gtime.modelos;

import com.trallerita.gtime.gtime.app.Libreria;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by mark_ on 23/04/2017.
 */

public class SubTarea extends RealmObject {

    // Valor para la BD con anotación Java
    @PrimaryKey
    private int id;
    @Required
    private String codigoSubTarea;
    @Required
    private Date fechaCreacion;
    private Date fechaFin;
    private Boolean horasPlanifControl;
    private long tiempoDesarrollo;
    private int horasPlanif;
    // "En curso" , "OK", "Inactiva"
    private String estadoSubTarea;

    public SubTarea (){

    }

    public SubTarea (String codigoSubTarea, boolean horasPlanifControl, int horasPlanif){
        this.id = Libreria.subTareaID.incrementAndGet();
        this.codigoSubTarea = codigoSubTarea;
        this.fechaCreacion =  new Date();
        this.horasPlanif = horasPlanif;
        this.horasPlanifControl = horasPlanifControl;
        this.tiempoDesarrollo = 0;
        this.estadoSubTarea = "Inactiva";
    }

    public int getId() {
        return id;
    }

    public String getCodigoSubTarea() {
        return codigoSubTarea;
    }

    public void setCodigoSubTarea(String codigoSubTarea) {
        this.codigoSubTarea = codigoSubTarea;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Boolean getHorasPlanifControl() {
        return horasPlanifControl;
    }

    public void setHorasPlanifControl(Boolean horasPlanifControl) {
        this.horasPlanifControl = horasPlanifControl;
    }

    public long getTiempoDesarrollo() {
        return tiempoDesarrollo;
    }

    public void setTiempoDesarrollo(long tiempoDesarrollo) {
        this.tiempoDesarrollo = tiempoDesarrollo;
    }

    public int getHorasPlanif() {
        return horasPlanif;
    }

    public void setHorasPlanif(int horasPlanif) {
        this.horasPlanif = horasPlanif;
    }

    public String getEstadoSubTarea() {
        return estadoSubTarea;
    }

    public void setEstadoSubTarea(String estadoSubTarea) {
        this.estadoSubTarea = estadoSubTarea;
    }
}