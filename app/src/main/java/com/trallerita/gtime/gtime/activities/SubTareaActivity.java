package com.trallerita.gtime.gtime.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.trallerita.gtime.gtime.R;
import com.trallerita.gtime.gtime.adaptadores.AdaptadorSubTarea;
import com.trallerita.gtime.gtime.app.Metodos;
import com.trallerita.gtime.gtime.modelos.SubTarea;
import com.trallerita.gtime.gtime.modelos.Tarea;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;

public class SubTareaActivity extends AppCompatActivity implements RealmChangeListener<Tarea> {

    private ListView listaSubTareas;
    private AdaptadorSubTarea adaptadorSubTarea;
    private RealmList<SubTarea> subTareas;
    private Realm realm;
    private int idTarea, horasPlanifInt, idEntorno,horasPlanifIntE;
    private Tarea tarea;
    public static int numSubTareasOK, numSubTareasTot;
    private  Bundle savedInstanceStateVar;
    Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_tarea);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        res = getResources();

        savedInstanceStateVar = savedInstanceState;
        numSubTareasTot = 0;
        numSubTareasOK = 0;

        realm = Realm.getDefaultInstance();

        if (getIntent().getExtras() != null){
            idTarea = getIntent().getExtras().getInt("idTarea");
            idEntorno = getIntent().getExtras().getInt("idEntorno");
        }

        tarea = realm.where(Tarea.class).equalTo("id", idTarea).findFirst();
        // Mira si hay cambios en la base de datos.
        tarea.addChangeListener(this);
        subTareas = tarea.getSubtareas();

        // Cambiamos el título de la activity
        this.setTitle(tarea.getCodigoTarea());

        listaSubTareas = (ListView) findViewById(R.id.lvListaSubTareas);
        adaptadorSubTarea = new AdaptadorSubTarea(this,R.layout.list_sub_tareas, subTareas);

        listaSubTareas.setAdapter(adaptadorSubTarea);

        clickarElementoLista(idTarea);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fbMasSubTarea);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                alertNuevaSubTarea(res.getString(R.string.subtarea_nueva),res.getString(R.string.subtarea_nueva_sub));

            }
        });

        // Para que funcione el editar / borrar ( manteniendo pulsado )
        registerForContextMenu(listaSubTareas);
    }

    // Método que crea un Dialog en la misma actividad para poder añadir una nueva SubTarea nueva
    private void alertNuevaSubTarea(String titulo, String mensaje){

        AlertDialog.Builder constructorAlert = new AlertDialog.Builder(this);

        if (titulo != null){
            constructorAlert.setTitle(titulo);
        }
        if (mensaje != null){
            constructorAlert.setMessage(mensaje);
        }

        View viewDialog = LayoutInflater.from(this).inflate(R.layout.dialog_nueva_sub_tarea, null);
        constructorAlert.setView(viewDialog);

        final EditText nombreSubTareaNuevo = (EditText) viewDialog.findViewById(R.id.etNombreNuevaSubTarea);
        final CheckBox controlHoras = (CheckBox) viewDialog.findViewById(R.id.cbControlHorasPlanif);
        final EditText horasPlanif = (EditText) viewDialog.findViewById(R.id.etHorasFlanifInt);

        // Control de la selección del checkbox
        // Cuando detecta el cambio, muestra el editText para determinar las horas planificadas
        controlHoras.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    horasPlanif.setVisibility(View.VISIBLE);
                }else{
                    horasPlanif.setVisibility(View.INVISIBLE);
                }
            }
        });

        constructorAlert.setPositiveButton(res.getString(R.string.anyadir), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String codigoSubTarea = nombreSubTareaNuevo.getText().toString().trim();
                String horasPlanifIni = horasPlanif.getText().toString().trim();

                if (horasPlanifIni.equals("")){
                    horasPlanifInt = 0;
                }else{
                    horasPlanifInt = Integer.parseInt(horasPlanifIni);
                }

                if (codigoSubTarea.length() > 0){
                    crearNuevaSubTarea(codigoSubTarea, controlHoras.isChecked(), horasPlanifInt);
                }else{
                    Toast.makeText(getApplicationContext(), res.getString(R.string.id_subtarea_vacio), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog dialog = constructorAlert.create();
        dialog.show();

    }

    private void alertEditarSubTarea(String titulo, String mensaje, final SubTarea subTarea){

        AlertDialog.Builder constructorAlert = new AlertDialog.Builder(this);

        if (titulo != null){
            constructorAlert.setTitle(titulo);
        }
        if (mensaje != null){
            constructorAlert.setMessage(mensaje);
        }

        View viewDialog = LayoutInflater.from(this).inflate(R.layout.dialog_nueva_sub_tarea, null);
        constructorAlert.setView(viewDialog);

        final EditText nombreSubTareaNuevo = (EditText) viewDialog.findViewById(R.id.etNombreNuevaSubTarea);
        final CheckBox controlHoras = (CheckBox) viewDialog.findViewById(R.id.cbControlHorasPlanif);
        final EditText horasPlanif = (EditText) viewDialog.findViewById(R.id.etHorasFlanifInt);

        // Control de la selección del checkbox
        // Cuando detecta el cambio, muestra el editText para determinar las horas planificadas
        controlHoras.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    horasPlanif.setVisibility(View.VISIBLE);
                }else{
                    horasPlanif.setVisibility(View.INVISIBLE);
                }
            }
        });

        nombreSubTareaNuevo.setText(subTarea.getCodigoSubTarea());
        controlHoras.setChecked(subTarea.getHorasPlanifControl());

        if (controlHoras.isChecked()){
            horasPlanif.setVisibility(View.VISIBLE);
            horasPlanif.setText(subTarea.getHorasPlanif()+"");
        }

        constructorAlert.setPositiveButton(res.getString(R.string.editar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String codigoSubTarea = nombreSubTareaNuevo.getText().toString().trim();
                String horasPlanifEdi = horasPlanif.getText().toString().trim();

                if (horasPlanifEdi.equals("")){
                    horasPlanifIntE = 0;
                }else{
                    horasPlanifIntE = Integer.parseInt(horasPlanifEdi);
                }

                if (codigoSubTarea.length() == 0){
                    Toast.makeText(getApplicationContext(), res.getString(R.string.id_subtarea_vacio), Toast.LENGTH_SHORT).show();
                }/*else if (codigoSubTarea.equals(subTarea.getCodigoSubTarea())){
                    Toast.makeText(getApplicationContext(), res.getString(R.string.mismo_nombre_subtarea), Toast.LENGTH_SHORT).show();
                }*/else{
                    editarSubTarea(subTarea,codigoSubTarea,controlHoras.isChecked(),horasPlanifIntE);
                }
            }
        });

        AlertDialog dialog = constructorAlert.create();
        dialog.show();

    }

    // Método que insertará en la Base de Datos un nuevo Entorno de Trabajo
    private void crearNuevaSubTarea(String codigoSubTarea, boolean controlHoras, int horasPlanifInt) {

        // Se hará mediante una transacción, para evitar problemas y errores.
        realm.beginTransaction();

        // Creamos el tarea y lo insertamos en la BD
        SubTarea subTarea = new SubTarea(codigoSubTarea, controlHoras, horasPlanifInt);
        realm.copyToRealm(subTarea);
        // De esta manera se guarda la relación para con el tarea.
        this.tarea.getSubtareas().add(subTarea);

        // Fin transacción
        realm.commitTransaction();
    }

    private void borrarTodo(){
        realm.beginTransaction();
        tarea.getSubtareas().deleteAllFromRealm();
        realm.commitTransaction();
        // Reinicia la actividad, asi pdemos ver los cambios al momento.
        //recreate();
    }

    private void borrarSubTarea(SubTarea subTarea){
        realm.beginTransaction();
        subTarea.deleteFromRealm();
        realm.commitTransaction();
        //recreate();
    }

    private void editarSubTarea(SubTarea subTarea, String nombreNuevo, boolean controlHoras, int horasPlanifIntE){
        realm.beginTransaction();
        subTarea.setCodigoSubTarea(nombreNuevo);
        subTarea.setHorasPlanifControl(controlHoras);
        subTarea.setHorasPlanif(horasPlanifIntE);
        realm.copyToRealmOrUpdate(subTarea);
        realm.commitTransaction();
        //recreate();
    }

    private void clickarElementoLista(final int idTarea){
        // Clickar sobre un elemento de la lista
        listaSubTareas.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Toast.makeText(TareaActivity.this,""+tareas.get(position),Toast.LENGTH_SHORT).show();
               Intent intent = new Intent(SubTareaActivity.this, CronoActivity.class);
               intent.putExtra("idTarea",idTarea);
               intent.putExtra("idSubTarea",subTareas.get(position).getId());
               intent.putExtra("idEntorno",idEntorno);
               intent.putExtra("numSubTareasOK",numSubTareasOK);
               intent.putExtra("numSubTareasTot",numSubTareasTot);
               startActivity(intent);

                Metodos.imprimirConsola(savedInstanceStateVar,"SubTareasTot",numSubTareasTot+"");
                Metodos.imprimirConsola(savedInstanceStateVar,"numSubTareasOK",numSubTareasOK+"");

            }

        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        menu.setHeaderTitle(subTareas.get(info.position).getCodigoSubTarea());
        getMenuInflater().inflate(R.menu.context_menu_subtareas_activity, menu);

        //super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.borrar_subtarea:
                borrarSubTarea(subTareas.get(info.position));
                return true;
            case R.id.editar_subtarea:
                alertEditarSubTarea(res.getString(R.string.editar_subtarea),res.getString(R.string.editar_subtarea_vamos), subTareas.get(info.position));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_subtareas, menu);
        //return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                return true;
            case R.id.borrar_todo:
                borrarTodo();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onChange(Tarea element) {
        adaptadorSubTarea.notifyDataSetChanged();
    }
}