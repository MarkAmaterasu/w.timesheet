package com.trallerita.gtime.gtime.modelos;

import com.trallerita.gtime.gtime.app.Libreria;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by mark_ on 04/04/2017.
 */

public class Tarea extends RealmObject {

    // Valor para la BD con anotación Java
    @PrimaryKey
    private int id;
    @Required
    private String codigoTarea;
    @Required
    private Date fechaCreacion;
    private Date fechaFin;
    @Required
    private Boolean subtareascontrol;
    private Boolean horasPlanifControl;
    private long tiempoDesarrollo;
    private int horasPlanif;
    // "En curso" , "OK", "Inactiva"
    private String estadoTarea;


    private RealmList<SubTarea> subtareas;



    public Tarea (){

    }

    public Tarea (String codigoTarea){
        this.id = Libreria.tareaID.incrementAndGet();
        this.codigoTarea = codigoTarea;
        this.fechaCreacion =  new Date();
        this.subtareascontrol = false;
        this.subtareas = new RealmList<SubTarea>();
    }

    public Tarea (String codigoTarea, boolean subtareascontrol, boolean horasPlanifControl, int horasPlanif ){
        this.id = Libreria.tareaID.incrementAndGet();
        this.codigoTarea = codigoTarea;
        this.fechaCreacion =  new Date();
        this.subtareascontrol = subtareascontrol;
        this.subtareas = new RealmList<SubTarea>();
        this.horasPlanif = horasPlanif;
        this.horasPlanifControl = horasPlanifControl;
        this.tiempoDesarrollo = 0;
        this.estadoTarea = "Inactiva";
    }

    public int getId() {
        return id;
    }

    public String getCodigoTarea() {
        return codigoTarea;
    }

    public void setCodigoTarea(String codigoTarea) {
        this.codigoTarea = codigoTarea;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public Boolean getSubtareascontrol() {
        return subtareascontrol;
    }

    public void setSubtareascontrol(Boolean subtareascontrol) {
        this.subtareascontrol = subtareascontrol;
    }

    public RealmList<SubTarea> getSubtareas() {
        return subtareas;
    }

    public long getTiempoDesarrollo() {
        return tiempoDesarrollo;
    }

    public void setTiempoDesarrollo(long tiempoDesarrollo) {
        this.tiempoDesarrollo = tiempoDesarrollo;
    }

    public int getHorasPlanif() {
        return horasPlanif;
    }

    public void setHorasPlanif(int horasPlanif) {
        this.horasPlanif = horasPlanif;
    }

    public String getEstadoTarea() {
        return estadoTarea;
    }

    public void setEstadoTarea(String estadoTarea) {
        this.estadoTarea = estadoTarea;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Boolean getHorasPlanifControl() {
        return horasPlanifControl;
    }

    public void setHorasPlanifControl(Boolean horasPlanifControl) {
        this.horasPlanifControl = horasPlanifControl;
    }
}
