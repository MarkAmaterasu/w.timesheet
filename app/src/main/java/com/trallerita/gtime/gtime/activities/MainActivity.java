package com.trallerita.gtime.gtime.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.trallerita.gtime.gtime.adaptadores.AdaptadorEntorno;
import com.trallerita.gtime.gtime.R;
import com.trallerita.gtime.gtime.modelos.Entorno;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

// Implementamos RealmChangeListener para mirar si surgen cambios en la base de datos. Hay que pasarle una lista de la clase que nos interesa.
public class MainActivity extends AppCompatActivity implements RealmChangeListener<RealmResults<Entorno>> {

    private ListView lvEntornos;
    private Realm realm;
    private RealmResults<Entorno> entornos;
    private AdaptadorEntorno adaptadorLista;
    Resources res;
    int progressPrioridad = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        res = getResources();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


           // Crea el Dialog / Alert
           alertNuevoEntorno(res.getString(R.string.entorno_nuevo),res.getString(R.string.entorno_nuevo_sub));

            }
        });

        // Cargamos la BD
        realm = Realm.getDefaultInstance();
        // Llenamos la lista con los datos de la base de datos. ( SELECT * FROM Entorno )
        entornos = realm.where(Entorno.class).findAll().sort("prioridad", Sort.DESCENDING);
        // Mira si hay cambios en la base de datos.
        entornos.addChangeListener(this);


        lvEntornos = (ListView) findViewById(R.id.lvEntornos);

        // Enlazamos con nuestro adaptador
        adaptadorLista = new AdaptadorEntorno(this,R.layout.list_entornos,entornos);
        lvEntornos.setAdapter(adaptadorLista);

        // Clickar sobre un elemento de la lista
        lvEntornos.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Toast.makeText(MainActivity.this,""+entornos.get(position),Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, TareaActivity.class);
                intent.putExtra("idEntorno",entornos.get(position).getId());
                startActivity(intent);

            }

        });

        // Para que funcione el editar / borrar ( manteniendo pulsado )
        registerForContextMenu(lvEntornos);

    }


    // Método que crea un Dialog en la misma actividad para poder añadir un Entorno de Trabajo nuevo
    private void alertNuevoEntorno(String titulo, String mensaje){

        AlertDialog.Builder constructorAlert = new AlertDialog.Builder(this);

        if (titulo != null){
            constructorAlert.setTitle(titulo);
        }
        if (mensaje != null){
            constructorAlert.setMessage(mensaje);
        }

        View viewDialog = LayoutInflater.from(this).inflate(R.layout.dialog_nuevo_entorno, null);
        constructorAlert.setView(viewDialog);

        final EditText nombreEntornoNuevo = (EditText) viewDialog.findViewById(R.id.etNombreNuevoEntorno);
        final SeekBar sbProyecto = (SeekBar) viewDialog.findViewById(R.id.sbProyecto);
        final TextView tvPrioridadProyecto = (TextView) viewDialog.findViewById(R.id.tvPrioridadProyecto);

        // Inicializamos el Textview a 0
        tvPrioridadProyecto.setText(sbProyecto.getProgress() + "/" + sbProyecto.getMax());

        sbProyecto.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progressPrioridad = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tvPrioridadProyecto.setText(progressPrioridad + "/" + seekBar.getMax());
            }
        });

        constructorAlert.setPositiveButton(res.getString(R.string.anyadir), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String nombreEntorno = nombreEntornoNuevo.getText().toString().trim();
                int prioridadProyecto = progressPrioridad;

                if (nombreEntorno.length() > 0){
                    crearNuevoEntorno(nombreEntorno, prioridadProyecto);
                }else{
                    Toast.makeText(getApplicationContext(), res.getString(R.string.nombre_entorno_vacio), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog dialog = constructorAlert.create();
        dialog.show();

    }

    private void alertEditarEntorno(String titulo, String mensaje, final Entorno entorno){

        AlertDialog.Builder constructorAlert = new AlertDialog.Builder(this);

        if (titulo != null){
            constructorAlert.setTitle(titulo);
        }
        if (mensaje != null){
            constructorAlert.setMessage(mensaje);
        }

        View viewDialog = LayoutInflater.from(this).inflate(R.layout.dialog_nuevo_entorno, null);
        constructorAlert.setView(viewDialog);

        final EditText nombreEntornoNuevo = (EditText) viewDialog.findViewById(R.id.etNombreNuevoEntorno);
        final SeekBar sbProyecto = (SeekBar) viewDialog.findViewById(R.id.sbProyecto);
        final TextView tvPrioridadProyecto = (TextView) viewDialog.findViewById(R.id.tvPrioridadProyecto);

        nombreEntornoNuevo.setText(entorno.getNombre());
        tvPrioridadProyecto.setText(entorno.getPrioridad() + "/" + sbProyecto.getMax());
        sbProyecto.setProgress(entorno.getPrioridad());

        sbProyecto.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progressPrioridad = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tvPrioridadProyecto.setText(progressPrioridad + "/" + seekBar.getMax());
            }
        });

        constructorAlert.setPositiveButton(res.getString(R.string.editar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String nombreEntorno = nombreEntornoNuevo.getText().toString().trim();
                int prioridadEdit = progressPrioridad;

                Toast.makeText(getApplicationContext(),  prioridadEdit+"", Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(),  entorno.getPrioridad()+"", Toast.LENGTH_SHORT).show();

                if (nombreEntorno.length() == 0){
                    Toast.makeText(getApplicationContext(), res.getString(R.string.nombre_entorno_vacio), Toast.LENGTH_SHORT).show();
                }else if (nombreEntorno.equals(entorno.getNombre()) && prioridadEdit == entorno.getPrioridad() ){
                    Toast.makeText(getApplicationContext(), res.getString(R.string.mismo_nombre), Toast.LENGTH_SHORT).show();
                }else{
                    editarEntorno(entorno,nombreEntorno,prioridadEdit);
                }

                /*if (nombreEntorno.length() > 0){
                    crearNuevoEntorno(nombreEntorno);
                }else{
                    Toast.makeText(getApplicationContext(), "Nombre de entorno vacío", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        AlertDialog dialog = constructorAlert.create();
        dialog.show();

    }

    // Método que insertará en la Base de Datos un nuevo Entorno de Trabajo
    private void crearNuevoEntorno(String nombreEntorno, int prioridad) {

        // Se hará mediante una transacción, para evitar problemas y errores.
        realm.beginTransaction();

        // Creamos el entorno y lo insertamos en la BD
        Entorno entorno = new Entorno(nombreEntorno, prioridad);
        realm.copyToRealm(entorno);

        // Fin transacción
        realm.commitTransaction();
    }

    private void borrarTodo(){
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        // Reinicia la actividad, asi pdemos ver los cambios al momento.
        //recreate();
    }

    private void borrarEntorno(Entorno entorno){
        realm.beginTransaction();
        entorno.deleteFromRealm();
        realm.commitTransaction();
        //recreate();
    }

    private void editarEntorno(Entorno entorno, String nombreNuevo, int prioridad){
        realm.beginTransaction();
        entorno.setNombre(nombreNuevo);
        entorno.setPrioridad(prioridad);
        realm.copyToRealmOrUpdate(entorno);
        realm.commitTransaction();
        //recreate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                return true;
            case R.id.borrar_todo:
                borrarTodo();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        menu.setHeaderTitle(entornos.get(info.position).getNombre());
        getMenuInflater().inflate(R.menu.context_menu_entorno_activity, menu);

        //super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.borrar_entorno:
                borrarEntorno(entornos.get(info.position));
                return true;
            case R.id.editar_entorno:
                alertEditarEntorno(res.getString(R.string.editar_entorno),res.getString(R.string.editar_entorno_vamos),entornos.get(info.position));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    // Método que notificará si hay algun cambio en la BD
    @Override
    public void onChange(RealmResults<Entorno> element) {
        adaptadorLista.notifyDataSetChanged();
    }
}

